/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.talha.controller;

import dev.talha.entities.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mahbuburrubtalha
 */
@Controller
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentRepo studentRepo;

    @RequestMapping("/add")
    public Student add(
            @RequestParam(value = "name", defaultValue = "talha") String name,
            @RequestParam(value = "reg_no", defaultValue = "123") String regNo
    ) {

        Student student = new Student();
        student.setName(name);
        student.setRegNo(regNo);
        studentRepo.save(student);

        return student;
    }

    @GetMapping("/all")
    public @ResponseBody Iterable<Student> getAllStudents() {
        return studentRepo.findAll();
    }
    
    @RequestMapping("/view_student")
    public String view_student(
            @RequestParam(value = "id") Long id, Model model
    ){
        
        Student student = studentRepo.findOne(id);
        model.addAttribute("student", student);
        return "view_student";
    }

}
