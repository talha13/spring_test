/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.talha.controller;

import dev.talha.entities.Student;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author mahbuburrubtalha
 */

public interface StudentRepo extends CrudRepository<Student, Long>{
    
}
